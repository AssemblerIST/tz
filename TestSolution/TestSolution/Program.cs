﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestSolution
{
    /// <summary>
    /// класс Program использует InfoSorting
    /// реализовано простейшее меню для пользователя.
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Program app = new Program();
            app.menu();
        }
        //простейшее меню
        void menu()
        {
            string path = "";
            Console.WriteLine("1 - Open File and start programm\n2 - Close programm");
           
            while (true)
            {
                try
                {
                    int value = Convert.ToInt32(Console.ReadLine());
                    switch (value)
                    {
                        case 1:
                            {
                                //создаю диалоговое окно для открытия фала
                                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                                openFileDialog1.InitialDirectory = @"C:\";
                                openFileDialog1.Title = "Browse Text Files";
                                openFileDialog1.CheckFileExists = true;
                                openFileDialog1.CheckPathExists = true;
                                openFileDialog1.DefaultExt = "txt";
                                openFileDialog1.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                                openFileDialog1.FilterIndex = 2;
                                openFileDialog1.ShowReadOnly = true;
                                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                                {
                                    path = openFileDialog1.FileName;
                                }
                                else break;
                                //копирую путь и создаю путь с файлом для записи
                                string pathWrite = Path.GetDirectoryName(path);
                                pathWrite += "\\result.txt";
                                //работа с классом InfoSorting
                                InfoSorting info = new InfoSorting();
                               
                                if (info.ReadFile(path))
                                {
                                    Console.WriteLine("Read is successful");
                                }
                                info.WriteToFile(pathWrite);
                                break;
                            }
                        case 2:
                            {
                                return;

                            }
                        default:
                            {
                                Console.WriteLine("Please, enter correct number\n Press 2 for exit from programm");
                                break;
                            }
                    }
                }
                catch (FormatException e)
                {
                    Console.WriteLine("Please, enter correct value");
                }                                           
            }
            
        }
    }
}
