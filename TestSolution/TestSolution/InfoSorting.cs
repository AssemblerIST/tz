﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestSolution
{
    ///<summary>
    ///структура myArray хранит данные о каждой строке
    ///считанном из файла
    ///</summary>

    struct myArray
    {
        public int ID;              //Id
        public int parentID;        // parent Id
        public string data;         //текст из файла
        public int dataLenght;      //длина текста (для сортировки по тексту)
    }

    /// <summary>
    /// класс InfoSorting считывает данные из файла, сортирует их по parentID,
    /// группирует данные по parentID,сортирует данные по тексту и записывает их в новый файл
    /// </summary>
    class InfoSorting
    {
//список для полученных данных из файла
        List<myArray> myList;
//список для сгрупированных и отсортированных данных на основе myList
        List<List<myArray>> sortedList;
        //инициализация переменных
        public InfoSorting()
        {
            this.myList = new List<myArray>();
            this.sortedList = new List<List<myArray>>();
        }
//чтение и запись данных из файла в 
        public bool ReadFile(string path)
        {
//обработка исключения, если файл не найден
            try
            {
                string[] array = File.ReadAllLines(path);

                foreach (string item in array) WriteList(item);

                SortByParentID(this.myList);
                WriteToSortedList();
                SortedInGroupList();
                return true;
            }
            catch(FileNotFoundException ex)
            {
                Console.WriteLine("Error!!! File is not found");
                return false;
            } 
        }
//запись в файл (генерируем новый список строк и записываем в файл отсортированные и сгруппированные данные
        public bool WriteToFile(string path)
        {
            List<string> str = new List<string>();
            foreach(List<myArray> item in this.sortedList)
            {
                foreach(myArray data in item)
                {
                   str.Add("|ID = " + (data.ID).ToString() + "|PId = " + (data.parentID).ToString() + "|Data = "+ data.data);
                }               
            }
            File.WriteAllLines(path, str, Encoding.UTF8);
            return true;
        }
//записывает данный из файла в список
        void WriteList(string str)
        {
            //проверка, если строка пустая
            if (String.IsNullOrEmpty(str)) return;

            myArray ptr = new myArray();
            //массив строк   -   ID | parentID | текст
            string[] s = str.Split('|');

            ptr.ID = Convert.ToInt32(s[0]);
            ptr.parentID = Convert.ToInt32(s[1]);
            //добавляем к данным количество пробелов равное индексу parentID
            ptr.data = "".PadLeft(ptr.parentID) + s[2];
            ptr.dataLenght = s[2].Length;
            this.myList.Add(ptr);
        }
//вывод на экран списка
        void PrintList(List<myArray> list)
        {
            foreach(myArray item in list)
            {
                Console.WriteLine("ID = {0} ParentId = {1} Data = {2}", item.ID, item.parentID, item.data);
            }
        }
//вывод на экран списка списков
        void PrintLists()
        {
            foreach(List<myArray> item in this.sortedList) PrintList(item);
        }
//сортировка данных по тексту (возвращает отсортированный список)
        List<myArray> SortByData(List<myArray> array)
        {
            var sortedData = from len in array
                             orderby len.dataLenght
                             select len;
            
            List<myArray> myList2 = new List<myArray>();
            foreach (myArray item in sortedData)   myList2.Add(item);
            return myList2;



        }
//сортировка данных по parentID и перезапись в myList
        void SortByParentID(List<myArray> array)
        {
            var sortedParentID = from pID in array
                             orderby pID.parentID
                             select pID;

            List<myArray> myList2 = new List<myArray>();

            foreach (myArray item in sortedParentID) myList2.Add(item);
            this.myList = myList2;
            //PrintList();
        }
//запись данных в список списков (группы списков)
        void WriteToSortedList()
        {
            //максимальная глубина дерева
            int maxPId = this.myList.Max(myArray => myArray.parentID);
            int currentPID = 0;
            //копия списка
            List<myArray> copyMyList = new List<myArray>();
            copyMyList.AddRange(this.myList);

            for (int i = 0; i <= maxPId; i++)
            {
                this.sortedList.Add(new List<myArray>());

                foreach(myArray item in this.myList)
                {   
                    //если текущий id == считываемому
                    if (currentPID == item.parentID)
                    {
                        //в список для группировки
                        this.sortedList[currentPID].Add(item);
                        copyMyList.Remove(item);
                    }
                    else continue;
                }
                currentPID += 1;
                //очищаем и обновляем список с данными
                this.myList.Clear();
                this.myList.AddRange(copyMyList);
            }
        }
//группировка данных в sortedList
        void SortedInGroupList()
        {
           
            //index для списка списков
            int index = 0;
            //создание копии для отсортированного списка групп
            List<List<myArray>> copySortedList = new List<List<myArray>>();
            copySortedList.AddRange(this.sortedList);

            foreach(List<myArray> item in sortedList)
            {          
                copySortedList[index] =  SortByData(item);
                index+=1;                
            }

            this.sortedList.Clear();
            this.sortedList.AddRange(copySortedList);
           
        }
    }
}
